import argparse
import importlib.util
import os

from shutil import copy
from os.path import join as pjoin
from typing import List, Tuple, Callable, Mapping, Any

import pandas as pd
import numpy as np

from prompt_toolkit import prompt
from catalyst.dl import utils


def dump_code(target_path: str, filename: str):
    filename_splitted = filename.split("/")

    if len(filename_splitted) > 1:
        gen_filename = filename_splitted[-2:]
        gen_filename = "___".join(gen_filename)
    else:
        gen_filename = filename_splitted[0]

    copy(filename, pjoin(target_path, gen_filename))


def collect_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument(
        "config", type=str, help="Path to .py file with CONFIG dict"
    )
    parser.add_argument(
        "--logdir_root",
        type=str,
        default="",
        help="Path to logdir root in your system",
    )

    args = parser.parse_args()

    return args


def extract_config_dict(path2config: str):

    spec = importlib.util.spec_from_file_location(
        name="module.name", location=path2config
    )
    config_dict = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(config_dict)
    print(f"Your config:\n{config_dict.CONFIG}")

    return config_dict.CONFIG


def load_df_and_split(path2df: str, path2split: str):
    return (pd.read_csv(path2df), np.load(path2split, allow_pickle=True))


def fix_rs(rs: int):
    utils.set_global_seed(rs)
    utils.prepare_cudnn(deterministic=True)


def create_logdir(exp_name: str, root: str = ""):
    logdir_name = pjoin(root, "logdirs", exp_name)
    logdir_exists = os.path.exists(logdir_name)
    if logdir_exists:
        answer = prompt(
            "Logdir already exists. Do you want to continue (y/n)? "
        )
        if answer.lower() != "y":
            raise RuntimeError(f"Folder {logdir_name} already exists!")
    else:
        os.makedirs(logdir_name)
        print("Folder created!")

    return logdir_name, logdir_exists


def dump_all_code(logdir_name: str, files_to_save: List[str]):
    code_folder_path = pjoin(logdir_name, "code")
    os.makedirs(code_folder_path)
    for f in files_to_save:
        dump_code(target_path=code_folder_path, filename=f)
    print("Code dumped!")


def run_cv_trainig(
    train_df: pd.DataFrame,
    folds: List[int],
    splits: List[Tuple[np.ndarray, np.ndarray]],
    logdir_name: str,
    train_function: Callable,
    train_function_args: Mapping[str, Any],
):
    for fold_num in folds:
        print(f"Fold {fold_num} preparation")
        split = splits[fold_num]
        train_train_df = train_df.iloc[split[0]]
        train_val_df = train_df.iloc[split[1]]
        print("Going into train function")
        train_function(
            train_df=train_train_df,
            val_df=train_val_df,
            exp_name=pjoin(logdir_name, f"fold_{fold_num}"),
            **train_function_args,
        )
        print(f"Fold {fold_num} training completed!")


if __name__ == "__main__":

    args = collect_args()

    config_dict = extract_config_dict(args.config)

    fix_rs(config_dict["seed"])

    train_df, splits = load_df_and_split(
        config_dict["df_path"], config_dict["split_path"]
    )

    logdir_name, logdir_exists = create_logdir(
        config_dict["exp_name"], args.logdir_root
    )

    if not logdir_exists:
        dump_all_code(logdir_name, config_dict["files_to_save"])

    run_cv_trainig(
        train_df=train_df,
        folds=config_dict["folds"],
        splits=splits,
        logdir_name=logdir_name,
        train_function=config_dict["train_function"],
        train_function_args=config_dict["train_function_args"],
    )

    print("END of main train script")
