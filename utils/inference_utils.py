import os

from typing import List, Optional, Callable, Mapping, Any
from collections import OrderedDict
from tqdm import tqdm

import numpy as np
import torch


def avarage_weights(
    nn_weights: List[OrderedDict],
    delete_module: bool = False,
    take_best: Optional[int] = None,
):
    if take_best is not None:
        print("solo model")
        avaraged_dict = OrderedDict()
        for k in nn_weights[take_best].keys():
            if delete_module:
                new_k = k[len("module.") :]
            else:
                new_k = k

            avaraged_dict[new_k] = nn_weights[take_best][k]
    else:
        n_nns = len(nn_weights)
        if n_nns < 2:
            raise RuntimeError("Please provide more then 2 checkpoints")

        avaraged_dict = OrderedDict()
        for k in nn_weights[0].keys():
            if delete_module:
                new_k = k[len("module.") :]
            else:
                new_k = k

            avaraged_dict[new_k] = sum(
                nn_weights[i][k] for i in range(n_nns)
            ) / float(n_nns)

    return avaraged_dict


def get_validation_models(
    model_initilizer: Callable,
    model_config: Mapping[str, Any],
    model_ckp_dicts: List[OrderedDict],
    device: str,
):
    t_models = []

    for mcd in model_ckp_dicts:

        t_model = model_initilizer(**model_config, device=device)
        t_model.load_state_dict(mcd)
        t_model = t_model.to(device)
        t_model.eval()
        t_models.append(t_model)

    return t_models


def create_val_loaders(
    loader_initilizer: object,
    loader_config: Mapping[str, Any],
    dfs: List[str],
    batch_size: int,
):
    t_loaders = []

    for df in dfs:
        t_dataset = loader_initilizer(df=df, **loader_config)
        t_loader = torch.utils.data.DataLoader(
            t_dataset,
            batch_size=batch_size,
            drop_last=False,
            shuffle=False,
            num_workers=os.cpu_count() // 2,
        )

        t_loaders.append(t_loader)

    return t_loaders


@torch.no_grad()
def cnn_model_predict(t_batch, t_model, t_device):
    image = t_batch[0].to(t_device)
    logits = t_model(image)
    logits = logits.detach().cpu().numpy()
    return logits


def predict_over_all_train(
    my_loaders, my_models, model_predict_func, device, do_concat=True
):
    logits = []
    for loader, model in zip(my_loaders, my_models):
        for batch in tqdm(loader):
            logit = model_predict_func(batch, model, device)
            logits.append(logit)

    if do_concat:
        logits = np.concatenate(logits)

    return logits
