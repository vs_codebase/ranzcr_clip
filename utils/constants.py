import numpy as np

CLASSES = [
    "ETT - Abnormal",
    "ETT - Borderline",
    "ETT - Normal",
    "NGT - Abnormal",
    "NGT - Borderline",
    "NGT - Incompletely Imaged",
    "NGT - Normal",
    "CVC - Abnormal",
    "CVC - Borderline",
    "CVC - Normal",
    "Swan Ganz Catheter Present",
]

CLASSES_CHESTX = [
    "Patient Age",
    "Patient Gender",
    "View Position",
    "Hernia",
    "Atelectasis",
    "Nodule",
    "Effusion",
    "Cardiomegaly",
    "Pleural_Thickening",
    "Consolidation",
    "Emphysema",
    "Mass",
    "Pneumonia",
    "Fibrosis",
    "Edema",
    "Pneumothorax",
]

DIALIATION_KERNEL = np.ones((9, 9), np.uint8)
