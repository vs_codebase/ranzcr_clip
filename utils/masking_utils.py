import warnings

import numpy as np
import cv2

from os.path import join as pjoin

from scipy import interpolate

from .constants import CLASSES, DIALIATION_KERNEL


def interpol_by_axis(x, y, do_revert=False):
    new_x, new_y = [], []
    for i in range(x.shape[0] - 1):

        f = interpolate.interp1d(x[i : i + 2], y[i : i + 2])
        xnew = np.arange(x[i : i + 2].min(), x[i : i + 2].max(), 1)
        fnew = f(xnew)

        new_x.append(xnew)
        new_y.append(fnew)

    if do_revert:
        return np.concatenate(
            [
                np.concatenate(new_y, axis=0)[:, None],
                np.concatenate(new_x, axis=0)[:, None],
            ],
            axis=-1,
        ).astype(int)
    else:
        return np.concatenate(
            [
                np.concatenate(new_x, axis=0)[:, None],
                np.concatenate(new_y, axis=0)[:, None],
            ],
            axis=-1,
        ).astype(int)


def interpolate_mask(temp_points):
    temp_points = np.array(eval(temp_points))

    return np.concatenate(
        [
            interpol_by_axis(
                temp_points[:, 1], temp_points[:, 0], do_revert=True
            ),
            interpol_by_axis(temp_points[:, 0], temp_points[:, 1]),
        ],
        axis=0,
    )


def read_image_shapes(filename: str):
    return cv2.imread(filename).shape


def board_points(point, shape):
    if point[0] >= shape[1]:
        point[0] = shape[1] - 1
    if point[1] >= shape[0]:
        point[1] = shape[0] - 1

    return point


def create_mask(temp_points, image_name):
    shape = read_image_shapes(image_name)[:-1]
    mask = np.zeros(shape)
    points = interpolate_mask(temp_points)
    points = np.array([board_points(el, shape) for el in points])
    mask[points[:, 1], points[:, 0]] = 1
    return mask


def create_tube_mask(
    filename, train_annotations, root, ext, save_by_channel=False
):
    sub_df = train_annotations[train_annotations.StudyInstanceUID == filename]
    full_name = pjoin(root, filename + ext)
    shape = read_image_shapes(full_name)

    if save_by_channel:
        mask = np.zeros((*shape[:-1], len(CLASSES)), dtype=np.uint8)
    else:
        mask = np.zeros((len(CLASSES), *shape[:-1]))

    for i in range(sub_df.shape[0]):
        cls_id = CLASSES.index(sub_df.iloc[i].label)
        one_mask = create_mask(sub_df.iloc[i].data, full_name)
        if save_by_channel:
            one_mask = (one_mask > 0).astype(np.uint8)
            one_mask = one_mask * 255
            one_mask = cv2.dilate(one_mask, DIALIATION_KERNEL, iterations=1)
            mask[:, :, cls_id] = one_mask
        else:
            mask[cls_id] = one_mask

    if save_by_channel:
        return mask
    else:
        mask = (mask.sum(0) > 0).astype(np.uint8)
        mask = mask * 255
        return cv2.dilate(mask, DIALIATION_KERNEL, iterations=1)
