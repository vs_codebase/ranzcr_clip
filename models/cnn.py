import sys

sys.path.append("../")

from typing import Optional, Mapping, Any

import torch.nn as nn
import torch.nn.functional as F
import torch
import geffnet
import timm

from .utils import (
    TaylorSoftmax,
    load_effnet_b5_start_point,
    load_resnet200d_start_point,
)

EFFNETB6_EMB_DIM = 2304
EFFNETB5_EMB_DIM = 2048
EFFNETB4_EMB_DIM = 1792
EFFNETB3_EMB_DIM = 1536
EFFNETB1_EMB_DIM = 1280
RESNET50_EMB_DIM = 2048
REXNET200_EMB_DIM = 2560
DENSENET121_EMB_DIM = 1024
NF_RESNET50_EMB_DIM = 2048

EPS = 1e-6


class CNNModel(nn.Module):
    def __init__(
        self,
        classifiier_config: Mapping[str, Any],
        encoder_type: str,
        device: str,
        use_pretrained_encoder: bool = True,
        path_to_chkp: Optional[str] = None,
        use_taylorsoftmax: bool = False,
        one_channel: bool = True,
    ):
        super().__init__()

        if path_to_chkp is not None:
            use_pretrained_encoder = False

        if encoder_type == "rexnet_200":
            self.encoder = timm.create_model(
                encoder_type, pretrained=use_pretrained_encoder
            )
            if one_channel:
                self.encoder.stem.conv.in_channels = 1
                weight = self.encoder.stem.conv.weight.mean(1, keepdim=True)
                self.encoder.stem.conv.weight = torch.nn.Parameter(weight)
            self.encoder.head.fc = nn.Identity()
            nn_embed_size = REXNET200_EMB_DIM
        elif encoder_type == "tf_efficientnet_b3_ns":
            self.encoder = timm.create_model(
                encoder_type, pretrained=use_pretrained_encoder
            )
            if one_channel:
                self.encoder.conv_stem.in_channels = 1
                weight = self.encoder.conv_stem.weight.mean(1, keepdim=True)
                self.encoder.conv_stem.weight = torch.nn.Parameter(weight)
            self.encoder.classifier = nn.Identity()
            nn_embed_size = EFFNETB3_EMB_DIM
        elif encoder_type == "tf_efficientnet_b5_ns":
            self.encoder = timm.create_model(
                encoder_type, pretrained=use_pretrained_encoder, num_classes=11
            )
            if path_to_chkp is not None:
                print("Loading starting point")
                state_dict = load_effnet_b5_start_point(path_to_chkp)
                self.encoder.load_state_dict(state_dict)
            if one_channel:
                self.encoder.conv_stem.in_channels = 1
                weight = self.encoder.conv_stem.weight.mean(1, keepdim=True)
                self.encoder.conv_stem.weight = torch.nn.Parameter(weight)
            self.encoder.classifier = nn.Identity()
            nn_embed_size = EFFNETB5_EMB_DIM
        elif encoder_type == "resnet200d":
            self.encoder = timm.create_model(
                encoder_type, pretrained=use_pretrained_encoder, num_classes=11
            )
            if path_to_chkp is not None:
                print("Loading starting point")
                state_dict = load_resnet200d_start_point(path_to_chkp)
                self.encoder.load_state_dict(state_dict)
            if one_channel:
                self.encoder.conv1[0].in_channels = 1
                weight = self.encoder.conv1[0].weight.mean(1, keepdim=True)
                self.encoder.conv1[0].weight = torch.nn.Parameter(weight)
            self.encoder.fc = nn.Identity()
            nn_embed_size = RESNET50_EMB_DIM
        else:
            raise ValueError(f"{encoder_type} is invalid model_type")

        classes_num = classifiier_config["classes_num"]
        hidden_dims = classifiier_config["hidden_dims"]
        second_dropout_rate = classifiier_config["second_dropout_rate"]
        if classifiier_config["classifier_type"] == "relu":
            first_dropout_rate = classifiier_config["first_dropout_rate"]
            self.classifier = nn.Sequential(
                nn.Linear(nn_embed_size, hidden_dims),
                nn.ReLU(),
                nn.Dropout(p=first_dropout_rate),
                nn.Linear(hidden_dims, hidden_dims),
                nn.ReLU(),
                nn.Dropout(p=second_dropout_rate),
                nn.Linear(hidden_dims, classes_num),
            )
        elif classifiier_config["classifier_type"] == "elu":
            first_dropout_rate = classifiier_config["first_dropout_rate"]
            self.classifier = nn.Sequential(
                nn.Dropout(first_dropout_rate),
                nn.Linear(nn_embed_size, hidden_dims),
                nn.ELU(),
                nn.Dropout(second_dropout_rate),
                nn.Linear(hidden_dims, classes_num),
            )
        elif classifiier_config["classifier_type"] == "dima":
            self.classifier = nn.Sequential(
                nn.BatchNorm1d(nn_embed_size),
                nn.Linear(nn_embed_size, hidden_dims),
                nn.BatchNorm1d(hidden_dims),
                nn.PReLU(hidden_dims),
                nn.Dropout(p=second_dropout_rate),
                nn.Linear(hidden_dims, classes_num),
            )
        elif classifiier_config["classifier_type"] == "prelu":
            first_dropout_rate = classifiier_config["first_dropout_rate"]
            self.classifier = nn.Sequential(
                nn.Dropout(first_dropout_rate),
                nn.Linear(nn_embed_size, hidden_dims),
                nn.PReLU(hidden_dims),
                nn.Dropout(p=second_dropout_rate),
                nn.Linear(hidden_dims, classes_num),
            )
        elif classifiier_config["classifier_type"] == "multiscale_relu":
            first_dropout_rate = classifiier_config["first_dropout_rate"]
            self.big_dropout = nn.Dropout(p=0.5)
            self.classifier = nn.Sequential(
                nn.Linear(nn_embed_size, hidden_dims),
                nn.ELU(),
                nn.Dropout(p=second_dropout_rate),
                nn.Linear(hidden_dims, classes_num),
            )
        elif classifiier_config["classifier_type"] == "drop_linear":
            self.classifier = nn.Sequential(
                nn.Dropout(p=second_dropout_rate),
                nn.Linear(nn_embed_size, classes_num),
            )
        else:
            raise ValueError("Invalid classifier_type")

        # Final activation
        self.use_taylorsoftmax = use_taylorsoftmax
        if self.use_taylorsoftmax:
            self.taylorsoftmax = TaylorSoftmax()
        # Classifier type
        self.classifier_type = classifiier_config["classifier_type"]
        # Some additional stuff
        self.encoder_type = encoder_type
        self.device = device
        self.to(self.device)

    def forward(self, image):
        x = self.encoder(image)

        if self.classifier_type == "multiscale_relu":
            logits = torch.mean(
                torch.stack(
                    [self.classifier(self.big_dropout(x)) for _ in range(5)],
                    dim=0,
                ),
                dim=0,
            )
        else:
            logits = self.classifier(x)

        if self.use_taylorsoftmax:
            logits = self.taylorsoftmax(logits).log()

        return logits
