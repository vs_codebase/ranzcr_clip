import sys

sys.path.append("../")

from typing import Optional, Mapping, Any
from collections import OrderedDict

import torch.nn as nn
import torch.nn.functional as F
import torch
import geffnet
import timm

import segmentation_models_pytorch as smp

from .utils import (
    TaylorSoftmax,
    load_effnet_b5_start_point,
    load_resnet200d_start_point,
    load_densenet121_start_point,
)

EFFNETB5_EMB_DIM = 512
DENSENET121_EMB_DIM = 1024


EPS = 1e-6


class CNNPreatrainModelSeg(nn.Module):
    def __init__(
        self,
        classifiier_config: Mapping[str, Any],
        encoder_type: str,
        encoder_config: Mapping[str, Any],
        path_to_chkp: str,
        device: str,
        enable_inference_mode: bool = False,
    ):
        super().__init__()

        chkp = torch.load(path_to_chkp, map_location="cpu")
        len_encdoder = len("encoder.")
        encoder_part = OrderedDict(
            {
                k[len_encdoder:]: v
                for k, v in chkp.items()
                if k.startswith("encoder")
            }
        )
        encoder_part["classifier.bias"] = None
        encoder_part["classifier.weight"] = None
        len_classifier = len("classifier.")
        classifier_part = OrderedDict(
            {
                k[len_classifier:]: v
                for k, v in chkp.items()
                if k.startswith("classifier")
            }
        )

        if encoder_type == "timm-efficientnet-b5_unet":
            self.encoder = smp.Unet(**encoder_config)
            self.encoder.encoder.load_state_dict(encoder_part)
            self.encoder.classification_head[3] = nn.Identity()
            nn_embed_size = EFFNETB5_EMB_DIM
        else:
            raise ValueError(f"{encoder_type} is invalid model_type")

        self.classifier = nn.Sequential(
            nn.Dropout(p=classifiier_config["second_dropout_rate"]),
            nn.Linear(nn_embed_size, classifiier_config["classes_num"]),
        )
        self.classifier.load_state_dict(classifier_part)

        self.main_classifier = nn.Sequential(
            nn.Dropout(classifiier_config["first_dropout_rate"]),
            nn.Linear(nn_embed_size, classifiier_config["hidden_dims"]),
            nn.ELU(),
            nn.Dropout(classifiier_config["second_dropout_rate"]),
            nn.Linear(
                classifiier_config["hidden_dims"],
                classifiier_config["main_classes_num"],
            ),
        )

        # Some additional stuff
        self.enable_inference_mode = enable_inference_mode
        self.encoder_type = encoder_type
        self.device = device
        self.to(self.device)

    def forward(self, image, enable_inference_mode=False):
        enable_inference_mode = (
            enable_inference_mode or self.enable_inference_mode
        )

        if enable_inference_mode:
            embs = self.encoder.encoder(image)
            x = self.encoder.classification_head(embs[-1])
        else:
            mask, x = self.encoder(image)

        main_logits = self.main_classifier(x)
        if not enable_inference_mode:
            additional_logits = self.classifier(x)

        if enable_inference_mode:
            return main_logits
        else:
            return mask, main_logits, additional_logits
