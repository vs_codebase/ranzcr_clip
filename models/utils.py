from collections import OrderedDict

import torch.nn as nn
import torch


class TaylorSoftmax(nn.Module):
    def __init__(self, dim=1, n=2):
        super(TaylorSoftmax, self).__init__()
        assert n % 2 == 0
        self.dim = dim
        self.n = n

    def forward(self, x):

        fn = torch.ones_like(x)
        denor = 1.0
        for i in range(1, self.n + 1):
            denor *= i
            fn = fn + x.pow(i) / denor
        out = fn / fn.sum(dim=self.dim, keepdims=True)
        return out


def load_effnet_b5_start_point(path: str):
    start_point = torch.load(path, map_location="cpu")

    new_start_point = OrderedDict()

    for k, v in start_point["model"].items():
        if k.startswith("classifier"):
            new_start_point[k] = v
        else:
            # Ignore `.module` at the begining
            new_start_point[k[6:]] = v

    return new_start_point


def load_resnet200d_start_point(path: str):
    start_point = torch.load(path, map_location="cpu")

    new_start_point = OrderedDict()

    for k, v in start_point["model"].items():
        if k.startswith("fc"):
            new_start_point[k] = v
        else:
            # Ignore `.module` at the begining
            new_start_point[k[6:]] = v

    return new_start_point


def load_densenet121_start_point(path: str):
    start_point = torch.load(path, map_location="cpu")

    new_start_point = OrderedDict()

    for k, v in start_point["model"].items():
        if k.startswith("classifier"):
            new_start_point[k] = v
        else:
            # Ignore `.module` at the begining
            new_start_point[k[6:]] = v

    return new_start_point
