import sys

sys.path.append("../")

from typing import Optional, Mapping, Any

import torch.nn as nn
import torch.nn.functional as F
import torch
import geffnet
import timm

import segmentation_models_pytorch as smp

from .utils import (
    TaylorSoftmax,
    load_effnet_b5_start_point,
    load_resnet200d_start_point,
    load_densenet121_start_point,
)

EFFNETB5_EMB_DIM = 512
DENSENET121_EMB_DIM = 1024


EPS = 1e-6


class CNNPreatrainModel(nn.Module):
    def __init__(
        self,
        classifiier_config: Mapping[str, Any],
        encoder_type: str,
        encoder_config: Mapping[str, Any],
        device: str,
    ):
        super().__init__()

        if encoder_type == "timm-efficientnet-b5_unet":
            unet = smp.Unet(**encoder_config)
            self.encoder = unet.encoder
            self.encoder_clf_head = unet.classification_head
            self.encoder_clf_head[3] = nn.Identity()
            nn_embed_size = EFFNETB5_EMB_DIM
        else:
            raise ValueError(f"{encoder_type} is invalid model_type")

        classes_num = classifiier_config["classes_num"]
        hidden_dims = classifiier_config["hidden_dims"]
        second_dropout_rate = classifiier_config["second_dropout_rate"]
        if classifiier_config["classifier_type"] == "relu":
            first_dropout_rate = classifiier_config["first_dropout_rate"]
            self.classifier = nn.Sequential(
                nn.Linear(nn_embed_size, hidden_dims),
                nn.ReLU(),
                nn.Dropout(p=first_dropout_rate),
                nn.Linear(hidden_dims, hidden_dims),
                nn.ReLU(),
                nn.Dropout(p=second_dropout_rate),
                nn.Linear(hidden_dims, classes_num),
            )
        elif classifiier_config["classifier_type"] == "elu":
            first_dropout_rate = classifiier_config["first_dropout_rate"]
            self.classifier = nn.Sequential(
                nn.Dropout(first_dropout_rate),
                nn.Linear(nn_embed_size, hidden_dims),
                nn.ELU(),
                nn.Dropout(second_dropout_rate),
                nn.Linear(hidden_dims, classes_num),
            )
        elif classifiier_config["classifier_type"] == "dima":
            self.classifier = nn.Sequential(
                nn.BatchNorm1d(nn_embed_size),
                nn.Linear(nn_embed_size, hidden_dims),
                nn.BatchNorm1d(hidden_dims),
                nn.PReLU(hidden_dims),
                nn.Dropout(p=second_dropout_rate),
                nn.Linear(hidden_dims, classes_num),
            )
        elif classifiier_config["classifier_type"] == "prelu":
            first_dropout_rate = classifiier_config["first_dropout_rate"]
            self.classifier = nn.Sequential(
                nn.Dropout(first_dropout_rate),
                nn.Linear(nn_embed_size, hidden_dims),
                nn.PReLU(hidden_dims),
                nn.Dropout(p=second_dropout_rate),
                nn.Linear(hidden_dims, classes_num),
            )
        elif classifiier_config["classifier_type"] == "multiscale_relu":
            first_dropout_rate = classifiier_config["first_dropout_rate"]
            self.big_dropout = nn.Dropout(p=0.5)
            self.classifier = nn.Sequential(
                nn.Linear(nn_embed_size, hidden_dims),
                nn.ELU(),
                nn.Dropout(p=second_dropout_rate),
                nn.Linear(hidden_dims, classes_num),
            )
        elif classifiier_config["classifier_type"] == "drop_linear":
            self.classifier = nn.Sequential(
                nn.Dropout(p=second_dropout_rate),
                nn.Linear(nn_embed_size, classes_num),
            )
        elif classifiier_config["classifier_type"] == "double_elu_mlp":
            first_dropout_rate = classifiier_config["first_dropout_rate"]
            self.classifier_prenet = nn.Sequential(
                nn.Dropout(first_dropout_rate),
                nn.Linear(nn_embed_size, hidden_dims),
                nn.ELU(),
            )
            self.classifier_hidden_class = nn.Sequential(
                nn.Dropout(second_dropout_rate),
                nn.Linear(
                    hidden_dims, classifiier_config["hidden_classes_num"]
                ),
            )
            self.classifier_class_final = nn.Sequential(
                nn.Dropout(second_dropout_rate),
                nn.Linear(
                    hidden_dims + classifiier_config["hidden_classes_num"],
                    classes_num,
                ),
            )
        else:
            raise ValueError("Invalid classifier_type")

        # Classifier type
        self.classifier_type = classifiier_config["classifier_type"]
        # Some additional stuff
        self.encoder_type = encoder_type
        self.device = device
        self.to(self.device)

    def forward(self, image):
        features = self.encoder(image)
        x = self.encoder_clf_head(features[-1])

        if self.classifier_type == "multiscale_relu":
            logits = torch.mean(
                torch.stack(
                    [self.classifier(self.big_dropout(x)) for _ in range(5)],
                    dim=0,
                ),
                dim=0,
            )
        else:
            logits = self.classifier(x)

        return logits
