import sys

sys.path.append("../")

from typing import Optional, Mapping, Any

import torch.nn as nn
import torch.nn.functional as F
import torch
import geffnet
import timm

import segmentation_models_pytorch as smp

from .utils import (
    TaylorSoftmax,
    load_effnet_b5_start_point,
    load_resnet200d_start_point,
    load_densenet121_start_point,
)

EFFNETB5_EMB_DIM = 512
DENSENET121_EMB_DIM = 1024


EPS = 1e-6


class CNNSegModel(nn.Module):
    def __init__(
        self,
        classifiier_config: Mapping[str, Any],
        encoder_type: str,
        encoder_config: Mapping[str, Any],
        device: str,
        path_to_chkp: Optional[str] = None,
        use_taylorsoftmax: bool = False,
        one_channel: bool = True,
        enable_inference_mode: bool = False,
    ):
        super().__init__()

        if path_to_chkp is not None:
            use_pretrained_encoder = False

        if encoder_type == "timm-efficientnet-b5_unet":
            self.encoder = smp.Unet(**encoder_config)
            if path_to_chkp is not None:
                print("Loading starting point")
                state_dict = load_effnet_b5_start_point(path_to_chkp)
                self.encoder.encoder.load_state_dict(state_dict)
            if one_channel:
                self.encoder.encoder.conv_stem.in_channels = 1
                weight = self.encoder.encoder.conv_stem.weight.mean(
                    1, keepdim=True
                )
                self.encoder.encoder.conv_stem.weight = torch.nn.Parameter(
                    weight
                )
            self.encoder.classification_head[3] = nn.Identity()
            nn_embed_size = EFFNETB5_EMB_DIM
        elif encoder_type == "densenet121_unet":
            self.encoder = smp.Unet(**encoder_config)
            if path_to_chkp is not None:
                print("Loading starting point")
                state_dict = load_densenet121_start_point(path_to_chkp)
                self.encoder.encoder.load_state_dict(state_dict)
            if one_channel:
                self.encoder.encoder.features.conv0.in_channels = 1
                weight = self.encoder.encoder.features.conv0.weight.mean(
                    1, keepdim=True
                )
                self.encoder.encoder.features.conv0.weight = torch.nn.Parameter(
                    weight
                )
            self.encoder.classification_head[3] = nn.Identity()
            nn_embed_size = DENSENET121_EMB_DIM
        else:
            raise ValueError(f"{encoder_type} is invalid model_type")

        classes_num = classifiier_config["classes_num"]
        hidden_dims = classifiier_config["hidden_dims"]
        second_dropout_rate = classifiier_config["second_dropout_rate"]
        if classifiier_config["classifier_type"] == "relu":
            first_dropout_rate = classifiier_config["first_dropout_rate"]
            self.classifier = nn.Sequential(
                nn.Linear(nn_embed_size, hidden_dims),
                nn.ReLU(),
                nn.Dropout(p=first_dropout_rate),
                nn.Linear(hidden_dims, hidden_dims),
                nn.ReLU(),
                nn.Dropout(p=second_dropout_rate),
                nn.Linear(hidden_dims, classes_num),
            )
        elif classifiier_config["classifier_type"] == "elu":
            first_dropout_rate = classifiier_config["first_dropout_rate"]
            self.classifier = nn.Sequential(
                nn.Dropout(first_dropout_rate),
                nn.Linear(nn_embed_size, hidden_dims),
                nn.ELU(),
                nn.Dropout(second_dropout_rate),
                nn.Linear(hidden_dims, classes_num),
            )
        elif classifiier_config["classifier_type"] == "dima":
            self.classifier = nn.Sequential(
                nn.BatchNorm1d(nn_embed_size),
                nn.Linear(nn_embed_size, hidden_dims),
                nn.BatchNorm1d(hidden_dims),
                nn.PReLU(hidden_dims),
                nn.Dropout(p=second_dropout_rate),
                nn.Linear(hidden_dims, classes_num),
            )
        elif classifiier_config["classifier_type"] == "prelu":
            first_dropout_rate = classifiier_config["first_dropout_rate"]
            self.classifier = nn.Sequential(
                nn.Dropout(first_dropout_rate),
                nn.Linear(nn_embed_size, hidden_dims),
                nn.PReLU(hidden_dims),
                nn.Dropout(p=second_dropout_rate),
                nn.Linear(hidden_dims, classes_num),
            )
        elif classifiier_config["classifier_type"] == "multiscale_relu":
            first_dropout_rate = classifiier_config["first_dropout_rate"]
            self.big_dropout = nn.Dropout(p=0.5)
            self.classifier = nn.Sequential(
                nn.Linear(nn_embed_size, hidden_dims),
                nn.ELU(),
                nn.Dropout(p=second_dropout_rate),
                nn.Linear(hidden_dims, classes_num),
            )
        elif classifiier_config["classifier_type"] == "drop_linear":
            self.classifier = nn.Sequential(
                nn.Dropout(p=second_dropout_rate),
                nn.Linear(nn_embed_size, classes_num),
            )
        elif classifiier_config["classifier_type"] == "double_elu_mlp":
            first_dropout_rate = classifiier_config["first_dropout_rate"]
            self.classifier_prenet = nn.Sequential(
                nn.Dropout(first_dropout_rate),
                nn.Linear(nn_embed_size, hidden_dims),
                nn.ELU(),
            )
            self.classifier_hidden_class = nn.Sequential(
                nn.Dropout(second_dropout_rate),
                nn.Linear(
                    hidden_dims, classifiier_config["hidden_classes_num"]
                ),
            )
            self.classifier_class_final = nn.Sequential(
                nn.Dropout(second_dropout_rate),
                nn.Linear(
                    hidden_dims + classifiier_config["hidden_classes_num"],
                    classes_num,
                ),
            )
        else:
            raise ValueError("Invalid classifier_type")

        # Final activation
        self.use_taylorsoftmax = use_taylorsoftmax
        if self.use_taylorsoftmax:
            self.taylorsoftmax = TaylorSoftmax()
        # Classifier type
        self.classifier_type = classifiier_config["classifier_type"]
        # Some additional stuff
        self.enable_inference_mode = enable_inference_mode
        self.encoder_type = encoder_type
        self.device = device
        self.to(self.device)

    def forward(self, image, enable_inference_mode=False):
        enable_inference_mode = (
            enable_inference_mode or self.enable_inference_mode
        )

        if enable_inference_mode:
            embs = self.encoder.encoder(image)
            x = self.encoder.classification_head(embs[-1])
        else:
            mask, x = self.encoder(image)

        if self.classifier_type == "multiscale_relu":
            logits = torch.mean(
                torch.stack(
                    [self.classifier(self.big_dropout(x)) for _ in range(5)],
                    dim=0,
                ),
                dim=0,
            )
        elif self.classifier_type == "double_elu_mlp":
            pre_logits = self.classifier_prenet(x)
            class_logits = self.classifier_hidden_class(pre_logits)
            logits = self.classifier_class_final(
                torch.cat([pre_logits, class_logits], axis=-1)
            )
        else:
            logits = self.classifier(x)

        if self.use_taylorsoftmax:
            logits = self.taylorsoftmax(logits).log()

        if enable_inference_mode:
            return logits
        else:
            if self.classifier_type == "double_elu_mlp":
                return mask, class_logits, logits
            else:
                return mask, logits
