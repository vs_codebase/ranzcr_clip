import sys

sys.path.append("../")

from typing import Optional, List, Tuple
from os.path import join as pjoin

import numpy as np
import pandas as pd
import torch

from PIL import Image
from copy import deepcopy


class RANZCRInferenceDataset(torch.utils.data.Dataset):
    def __init__(
        self,
        df,
        root,
        ext,
        path_col,
        transforms=None,
        augmentations=None,
        mask_root=None,
        mask_ext=".npy",
    ):

        super().__init__()
        df = df.reset_index(drop=True).copy()
        self.transforms = transforms
        self.augmentations = augmentations

        self.image_names = self._prepare_image_names(
            df[path_col].tolist(), root, ext
        )

        if mask_root is not None:
            self.mask_names = self._prepare_image_names(
                df[path_col].tolist(), mask_root, mask_ext
            )
            self.use_mask = True
        else:
            self.mask_names = None
            self.use_mask = False

    def __len__(self):
        return len(self.image_names)

    def _prepare_image_names(self, basenames: List[str], root: str, ext: str):
        return [pjoin(root, el) + ext for el in basenames]

    def _prepare_img_target_from_idx(self, idx: int):

        image_name = self.image_names[idx]

        img = Image.open(image_name)
        img = np.array(img)

        if self.use_mask:
            mask = np.load(self.mask_names[idx]).transpose(1, 2, 0)

        if self.augmentations is not None:
            if self.use_mask:
                res = self.augmentations(image=img, mask=mask)
                img = res["image"]
                mask = res["mask"]
            else:
                img = self.augmentations(image=img)["image"]

        if self.transforms is not None:
            img = self.transforms(img)
            if self.use_mask:
                mask = self.transforms(mask)

        if self.use_mask:
            img = torch.cat([img, mask], dim=0)

        return img

    def __getitem__(self, index: int):

        img = self._prepare_img_target_from_idx(index)

        return img
