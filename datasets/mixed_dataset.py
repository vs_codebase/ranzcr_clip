import sys
import os

sys.path.append("../")

from typing import Optional, List, Tuple
from os.path import join as pjoin
from tqdm import tqdm

import numpy as np
import pandas as pd
import torch

from PIL import Image
from copy import deepcopy

from utils.constants import CLASSES


class RANZCRMixedDataset(torch.utils.data.Dataset):
    def __init__(
        self,
        df,
        root_img,
        root_tube,
        root_lung,
        ext,
        path_col,
        pseudo_df_path=None,
        pseudo_root=None,
        multi_channel=False,
        hidden_target=False,
        mask_to_memmory=False,
        pixel_thresh=128,
        target_col=CLASSES,
        transforms=None,
        augmentations=None,
    ):

        super().__init__()
        df = df.reset_index(drop=True).copy()
        self.transforms = transforms
        self.augmentations = augmentations
        self.pixel_thresh = pixel_thresh
        self.multi_channel = multi_channel
        self.mask_to_memmory = mask_to_memmory
        self.hidden_target = hidden_target
        self.use_pseudo = (
            pseudo_df_path is not None and pseudo_root is not None
        )
        if self.use_pseudo:
            pseudo_df = pd.read_csv(pseudo_df_path)

        self.image_names = self._prepare_image_names(
            df[path_col].tolist(), root_img, ext
        )
        self.lung_names = self._prepare_image_names(
            df[path_col].tolist(), root_lung, ext
        )
        self.tube_names = self._prepare_image_names(
            df[path_col].tolist(), root_tube, ".npy" if multi_channel else ext
        )
        if self.use_pseudo:
            self.is_pseudo = [False] * len(self.image_names) + [True] * len(
                pseudo_df
            )
            self.image_names = self.image_names + self._prepare_image_names(
                pseudo_df[path_col].tolist(), pseudo_root, ext
            )
        if mask_to_memmory:
            if multi_channel:
                self.all_tubes = {
                    name: np.load(name)
                    for name in tqdm(self.tube_names)
                    if os.path.exists(name)
                }
            else:
                self.all_tubes = {
                    name: Image.open(name)
                    for name in tqdm(self.tube_names)
                    if os.path.exists(name)
                }

            self.all_images = {
                name: np.array(Image.open(name))
                for name in tqdm(self.image_names)
            }
            self.all_lungs = {
                name: np.array(Image.open(name))
                for name in tqdm(self.lung_names)
            }

        self.labels = df[target_col].values
        if self.use_pseudo:
            self.labels = np.concatenate(
                [self.labels, pseudo_df[target_col].values], axis=0
            )
        if self.hidden_target:
            df["ETT"] = (
                df[["ETT - Abnormal", "ETT - Borderline", "ETT - Normal"]].sum(
                    1
                )
                > 0
            ).astype(int)
            df["NGT"] = (
                df[
                    [
                        "NGT - Abnormal",
                        "NGT - Borderline",
                        "NGT - Incompletely Imaged",
                        "NGT - Normal",
                    ]
                ].sum(1)
                > 0
            ).astype(int)
            df["CVC"] = (
                df[["CVC - Abnormal", "CVC - Borderline", "CVC - Normal"]].sum(
                    1
                )
                > 0
            ).astype(int)
            self.hidden_labels = df[
                ["ETT", "NGT", "CVC", "Swan Ganz Catheter Present"]
            ].values

    def __len__(self):
        return len(self.image_names)

    def _prepare_image_names(self, basenames: List[str], root: str, ext: str):
        return [pjoin(root, el) + ext for el in basenames]

    def _prepare_img_target_from_idx(self, idx: int):

        image_name = self.image_names[idx]
        target = self.labels[idx]
        target = torch.from_numpy(target)
        if self.use_pseudo and self.is_pseudo[idx]:
            # Pseudo branch
            img = Image.open(image_name)
            img = np.array(img)
            if self.augmentations is not None:
                res = self.augmentations(image=img)["image"]
            if self.transforms is not None:
                img = self.transforms(img)
            return (
                img,
                target,
                torch.zeros(*img.shape[1:]).float(),
                torch.zeros(*img.shape[1:]).float(),
                False,
                False,
            )
        else:
            # Real branch
            lung_name = self.lung_names[idx]
            tube_name = self.tube_names[idx]

            if self.hidden_target:
                hidden_target = self.hidden_labels[idx]
                hidden_target = torch.from_numpy(hidden_target)

            if not self.mask_to_memmory:
                img = Image.open(image_name)
                lung = Image.open(lung_name)

            if os.path.exists(tube_name):
                if self.mask_to_memmory:
                    tube = self.all_tubes[tube_name]
                else:
                    if self.multi_channel:
                        tube = np.load(tube_name)
                    else:
                        tube = Image.open(tube_name)
            else:
                tube = None

            if self.mask_to_memmory:
                img = self.all_images[image_name]
                lung = self.all_lungs[lung_name]
            else:
                img = np.array(img)
                lung = np.array(lung)

            if tube is not None and not self.multi_channel:
                tube = np.array(tube)

            mask_lung = (lung > self.pixel_thresh).astype(float)[..., None]

            if tube is not None:
                mask_tube = (tube > self.pixel_thresh).astype(float)
                if not self.multi_channel:
                    mask_tube = mask_tube[..., None]
            else:
                if self.multi_channel:
                    mask_tube = np.zeros((*mask_lung.shape[:-1], len(CLASSES)))
                else:
                    mask_tube = np.zeros_like(mask_lung)

            mask = np.concatenate([mask_lung, mask_tube], axis=-1)

            if self.augmentations is not None:
                res = self.augmentations(image=img, mask=mask)
                img = res["image"]
                mask = res["mask"]

            if self.transforms is not None:
                img = self.transforms(img)
                mask = (self.transforms(mask) > 0.5).float()

                mask_lung = mask[0, :, :]
                if self.multi_channel:
                    mask_tube = mask[1:, :, :]
                else:
                    mask_tube = mask[1, :, :]
            else:
                mask_lung = mask[:, :, 0]
                if self.multi_channel:
                    mask_tube = mask_tube[:, :, 1:]
                else:
                    mask_tube = mask[:, :, 1]

            has_tube = tube is not None

            # The last is `has_lung`
            if self.hidden_target:
                return (
                    img,
                    target,
                    hidden_target,
                    mask_lung,
                    mask_tube,
                    has_tube,
                    True,
                )
            else:
                return img, target, mask_lung, mask_tube, has_tube, True

    def __getitem__(self, index: int):

        return self._prepare_img_target_from_idx(index)
