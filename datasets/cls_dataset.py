import sys

sys.path.append("../")

from typing import Optional, List, Tuple
from os.path import join as pjoin

import numpy as np
import pandas as pd
import torch

from PIL import Image
from copy import deepcopy

from utils.fmix_utils import sample_mask, make_low_freq_image, binarise_mask
from utils.constants import CLASSES
from .utils import rand_bbox


class RANZCRDataset(torch.utils.data.Dataset):
    def __init__(
        self,
        df,
        root,
        ext,
        path_col,
        target_col=CLASSES,
        mask_root=None,
        mask_ext=".npy",
        transforms=None,
        augmentations=None,
        do_fmix=False,
        fmix_params={
            "prob": 0.5,
            "alpha": 1.0,
            "decay_power": 3.0,
            "max_soft": True,
            "reformulate": False,
        },
        do_mixup=False,
        mixup_params={"prob": 0.5, "alpha": 1.0},
        do_cutmix=False,
        cutmix_params={"prob": 0.5, "alpha": 1.0},
    ):

        super().__init__()
        df = df.reset_index(drop=True).copy()
        self.transforms = transforms
        self.augmentations = augmentations
        self.root = root
        self.do_fmix = do_fmix
        self.fmix_params = fmix_params
        self.do_cutmix = do_cutmix
        self.cutmix_params = cutmix_params
        self.do_mixup = do_mixup
        self.mixup_params = mixup_params

        self.image_names = self._prepare_image_names(
            df[path_col].tolist(), root, ext
        )
        if mask_root is not None:
            self.mask_names = self._prepare_image_names(
                df[path_col].tolist(), mask_root, mask_ext
            )
            self.use_mask = True
        else:
            self.mask_names = None
            self.use_mask = False

        self.labels = df[target_col].values

    def __len__(self):
        return len(self.image_names)

    def _prepare_image_names(self, basenames: List[str], root: str, ext: str):
        return [pjoin(root, el) + ext for el in basenames]

    def _prepare_img_target_from_idx(self, idx: int):

        image_name = self.image_names[idx]
        target = self.labels[idx]
        target = torch.from_numpy(target).float()

        img = Image.open(image_name)
        img = np.array(img)

        if self.use_mask:
            mask = np.load(self.mask_names[idx]).transpose(1, 2, 0)

        if self.augmentations is not None:
            if self.use_mask:
                res = self.augmentations(image=img, mask=mask)
                img = res["image"]
                mask = res["mask"]
            else:
                img = self.augmentations(image=img)["image"]

        if self.transforms is not None:
            img = self.transforms(img)
            if self.use_mask:
                mask = self.transforms(mask)

        if self.use_mask:
            img = torch.cat([img, mask], dim=0)

        # if self.mask_names is not None:
        #     mask = torch.from_numpy(np.load(self.mask_names[idx]))
        #     img = torch.cat([img, mask], dim=0)

        return img, target

    def __getitem__(self, index: int):

        img, target = self._prepare_img_target_from_idx(index)

        if len(img.shape) == 3:
            is_numpy = False
            im_shapes = (img.shape[1], img.shape[2])
        else:
            is_numpy = True
            im_shapes = (img.shape[0], img.shape[1])

        if self.do_mixup and np.random.binomial(
            n=1, p=self.mixup_params["prob"]
        ):
            lam = np.random.beta(
                self.mixup_params["alpha"], self.mixup_params["alpha"]
            )

            mixup_ix = np.random.randint(0, self.__len__())
            mixup_img, mixup_target = self._prepare_img_target_from_idx(
                mixup_ix
            )

            img = img * lam + (1.0 - lam) * mixup_img

            target = target * lam + (1.0 - lam) * mixup_target

            if not is_numpy:
                img = img.float()

        if self.do_fmix and np.random.binomial(
            n=1, p=self.fmix_params["prob"]
        ):
            lam = np.clip(
                np.random.beta(
                    self.fmix_params["alpha"], self.fmix_params["alpha"]
                ),
                0.6,
                0.7,
            )

            # Make mask, get mean / std
            mask = make_low_freq_image(
                self.fmix_params["decay_power"], (im_shapes[0], im_shapes[1])
            )
            mask = binarise_mask(
                mask,
                lam,
                (im_shapes[0], im_shapes[1]),
                self.fmix_params["max_soft"],
            )

            if is_numpy:
                mask = mask.transpose(1, 2, 0)
            else:
                mask = torch.from_numpy(mask)

            fmix_ix = np.random.randint(0, self.__len__())
            fmix_img, fmix_target = self._prepare_img_target_from_idx(fmix_ix)

            # mix image
            img = mask * img + (1.0 - mask) * fmix_img
            if not is_numpy:
                img = img.float()

            # mix target
            rate = mask.sum() / (im_shapes[0] * im_shapes[1])
            target = rate * target + (1.0 - rate) * fmix_target

        if self.do_cutmix and np.random.binomial(
            n=1, p=self.cutmix_params["prob"]
        ):
            cmix_ix = np.random.randint(0, self.__len__())
            cmix_img, cmix_target = self._prepare_img_target_from_idx(cmix_ix)

            lam = np.random.beta(
                self.cutmix_params["alpha"], self.cutmix_params["alpha"]
            )
            bbx1, bby1, bbx2, bby2 = rand_bbox(
                (im_shapes[0], im_shapes[1]), lam
            )

            img[:, bbx1:bbx2, bby1:bby2] = cmix_img[:, bbx1:bbx2, bby1:bby2]

            rate = 1 - (
                ((bbx2 - bbx1) * (bby2 - bby1)) / (im_shapes[0] * im_shapes[1])
            )
            target = rate * target + (1.0 - rate) * cmix_target

        return img, target
