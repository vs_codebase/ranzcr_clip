from copy import deepcopy
from typing import Mapping, Any

import torch.nn as nn
import torch.nn.functional as F
import torch
import numpy as np

import segmentation_models_pytorch as smp


class LungTubeDice(nn.Module):
    def __init__(self, lung_coef: float = 1.0, tube_coef: float = 1.0):
        super().__init__()

        self.lung_coef = lung_coef
        self.tube_coef = tube_coef

        self.dice_loss = smp.utils.losses.DiceLoss()

    def forward(self, x, model):

        image, mask = x

        pred_mask = model(image)

        loss_lung = self.dice_loss(pred_mask[:, 0, :, :], mask[:, 0, :, :])
        loss_tube = self.dice_loss(pred_mask[:, 1, :, :], mask[:, 1, :, :])

        loss = loss_lung * self.lung_coef + loss_tube * self.tube_coef

        loss_dict = dict(loss=loss, dice_lung=loss_lung, dice_tube=loss_tube)

        return loss_dict, mask, pred_mask
