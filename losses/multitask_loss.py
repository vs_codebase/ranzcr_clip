from copy import deepcopy
from typing import Mapping, Any

import torch.nn as nn
import torch.nn.functional as F
import torch
import numpy as np

import segmentation_models_pytorch as smp

from .utils import label_smoothing


class MultiTaskLoss(nn.Module):
    def __init__(
        self,
        lung_coef: float = 1.0,
        tube_coef: float = 1.0,
        bce_coef: float = 2.0,
        additional_bce_coef: float = 1.0,
        additional_l1_coef: float = 1 / 16,
        use_label_smotthing: bool = False,
        label_smoothing_coef: float = 0.05,
        lr_thresh=0.0001,
    ):
        super().__init__()

        self.lung_coef = lung_coef
        self.tube_coef = tube_coef
        self.bce_coef = bce_coef
        self.additional_bce_coef = additional_bce_coef
        self.additional_l1_coef = additional_l1_coef

        self.use_label_smotthing = use_label_smotthing
        self.label_smoothing_coef = label_smoothing_coef
        self.lr_thresh = lr_thresh

        self.bce_loss = nn.BCEWithLogitsLoss()
        self.dice_loss = smp.utils.losses.DiceLoss()
        self.l1_loss = nn.L1Loss()

    def forward(self, x, model, lr=None):

        image, target, mask_lung, mask_tube, has_tube = x

        target_main = target[:, :11]
        target_cont = target[:, 11:12]
        target_additional = target[:, 12:]

        pred_mask, pred_label, pred_label_additional = model(image)

        # Compute lung dice
        loss_lung = self.dice_loss(pred_mask[:, 0, :, :], mask_lung)
        # Compute tube dice
        selected_pred_tube = pred_mask[has_tube]
        selected_real_tube = mask_tube[has_tube]
        if selected_pred_tube.shape[0] > 0:
            loss_tube = self.dice_loss(
                selected_pred_tube[:, 1, :, :], selected_real_tube
            )
        else:
            loss_tube = 0

        # Compute BCE final
        if self.label_smoothing_coef > 0 and model.training:
            loss_target_main = label_smoothing(
                target_main.float(), smoothing=self.label_smoothing_coef
            )
        else:
            loss_target_main = target_main.float()

        loss_bce = self.bce_loss(pred_label, loss_target_main)

        # Compute additional losses
        if self.label_smoothing_coef > 0 and model.training:
            loss_target_additional = label_smoothing(
                target_additional.float(), smoothing=self.label_smoothing_coef
            )
        else:
            loss_target_additional = target_additional.float()

        loss_bce_additional = self.bce_loss(
            pred_label_additional[:, 1:], loss_target_additional
        )
        l1 = self.l1_loss(pred_label_additional[:, :1], target_cont)

        if lr is not None and lr < self.lr_thresh:
            loss = loss_bce
        else:
            loss = (
                loss_lung * self.lung_coef
                + loss_tube * self.tube_coef
                + loss_bce * self.bce_coef
                + loss_bce_additional * self.additional_bce_coef
                + l1 * self.additional_l1_coef
            )

        loss_dict = dict(
            loss=loss,
            dice_lung=loss_lung,
            dice_tube=loss_tube,
            bce=loss_bce,
            additional_bce=loss_bce_additional,
            additional_l1=l1,
        )

        mask = torch.stack([mask_lung, mask_tube], dim=1)

        return loss_dict, mask, pred_mask, target_main, pred_label
