from typing import Tuple

import torch
import torch.nn as nn
import numpy as np

from catalyst.contrib.nn import FocalLossBinary

from .utils import label_smoothing


class StrongCatalystFocal(nn.Module):
    def __init__(
        self,
        gamma: float = 2.0,
        alpha: float = 0.25,
        label_smoothing: float = 0.0,
    ):
        super().__init__()
        self.loss_f = FocalLossBinary(gamma=gamma, alpha=alpha)
        self.label_smoothing = label_smoothing

    def forward(self, batch, model):

        image, target = batch

        logits = model(image)

        if self.label_smoothing > 0 and model.training:
            loss_target = label_smoothing(
                target.float(), smoothing=self.label_smoothing
            )
        else:
            loss_target = target.float()

        loss = self.loss_f(logits, loss_target)

        return loss, target, logits
