from copy import deepcopy
from typing import Mapping, Any

import torch.nn as nn
import torch.nn.functional as F
import torch
import numpy as np

from .utils import label_smoothing


class StrongBCEL1(nn.Module):
    def __init__(
        self,
        use_label_smotthing: bool = False,
        label_smoothing_coef: float = 0.05,
        bce_coef: float = 16,
    ):
        super().__init__()

        self.use_label_smotthing = use_label_smotthing
        self.label_smoothing_coef = label_smoothing_coef

        self.bce_loss = nn.BCEWithLogitsLoss()
        self.l1_loss = nn.L1Loss()

        self.bce_coef = bce_coef

    def forward(self, x, model):

        image, target = x

        target, target_cont = target[:, 1:], target[:, :1]

        if self.label_smoothing_coef > 0 and model.training:
            loss_target = label_smoothing(
                target.float(), smoothing=self.label_smoothing_coef
            )
        else:
            loss_target = target.float()

        logits = model(image)

        loss_bce = self.bce_loss(logits[:, 1:], loss_target)
        l1 = self.l1_loss(logits[:, :1], target_cont)

        loss = {
            "bce": loss_bce,
            "l1": l1,
            "loss": loss_bce * self.bce_coef + l1,
        }

        return loss, target, logits[:, 1:]
