import torch


def label_smoothing(
    target: torch.Tensor, smoothing: float = 0.05, margin: float = 0.5
):
    target = target.clone()
    high_margin_mask = target > (margin + smoothing)
    low_margin_mask = target < (margin - smoothing)
    target[high_margin_mask] = target[high_margin_mask] - smoothing
    target[low_margin_mask] = target[low_margin_mask] + smoothing
    return target
