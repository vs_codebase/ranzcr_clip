from copy import deepcopy
from typing import Mapping, Any

import torch.nn as nn
import torch.nn.functional as F
import torch
import numpy as np

from .utils import label_smoothing


class StrongBCE(nn.Module):
    def __init__(
        self,
        use_label_smotthing: bool = False,
        label_smoothing_coef: float = 0.05,
    ):
        super().__init__()

        self.use_label_smotthing = use_label_smotthing
        self.label_smoothing_coef = label_smoothing_coef

        self.bce_loss = nn.BCEWithLogitsLoss()

    def forward(self, x, model):

        image, target = x

        if self.label_smoothing_coef > 0 and model.training:
            loss_target = label_smoothing(
                target.float(), smoothing=self.label_smoothing_coef
            )
        else:
            loss_target = target.float()

        logits = model(image)

        loss = self.bce_loss(logits, loss_target)

        loss = dict(loss=loss)

        return loss, target, logits
