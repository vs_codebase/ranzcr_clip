from copy import deepcopy
from typing import Mapping, Any

import torch.nn as nn
import torch.nn.functional as F
import torch
import numpy as np

import segmentation_models_pytorch as smp

from .utils import label_smoothing


class LungTubeDiceLabelBCE(nn.Module):
    def __init__(
        self,
        lung_coef: float = 1.0,
        tube_coef: float = 1.0,
        bce_coef: float = 2.0,
        hidden_bce_coef: float = 1.0,
        use_label_smotthing: bool = False,
        label_smoothing_coef: float = 0.05,
        lr_thresh=0.0001,
        multi_channel: bool = False,
        double_mlp: bool = False,
    ):
        super().__init__()

        self.lung_coef = lung_coef
        self.tube_coef = tube_coef
        self.bce_coef = bce_coef
        self.hidden_bce_coef = hidden_bce_coef

        self.dice_loss = smp.utils.losses.DiceLoss()

        self.use_label_smotthing = use_label_smotthing
        self.label_smoothing_coef = label_smoothing_coef
        self.lr_thresh = lr_thresh
        self.multi_channel = multi_channel
        self.double_mlp = double_mlp

        self.bce_loss = nn.BCEWithLogitsLoss()

    def forward(self, x, model, lr=None):

        if self.double_mlp:
            (
                image,
                target,
                target_hidden,
                mask_lung,
                mask_tube,
                has_tube,
                has_lung,
            ) = x
        else:
            image, target, mask_lung, mask_tube, has_tube, has_lung = x

        if self.double_mlp:
            pred_mask, pred_label_hidden, pred_label = model(image)
        else:
            pred_mask, pred_label = model(image)

        # Compute lung dice
        selected_pred_lung = pred_mask[has_lung]
        selected_real_lung = mask_lung[has_lung]
        if selected_pred_lung.shape[0] > 0:
            loss_lung = self.dice_loss(
                selected_pred_lung[:, 0, :, :], selected_real_lung
            )
        else:
            loss_lung = 0
        # Compute tube dice
        selected_pred_tube = pred_mask[has_tube]
        selected_real_tube = mask_tube[has_tube]
        if selected_pred_tube.shape[0] > 0:
            if self.multi_channel:
                print("AAAAAAAAAAAAAAAAAAAAAAAA")
                loss_tube_ett_abnormal = self.dice_loss(
                    selected_pred_tube[:, 1, :, :],
                    selected_real_tube[:, 0, :, :],
                )
                loss_tube_ett_borderline = self.dice_loss(
                    selected_pred_tube[:, 2, :, :],
                    selected_real_tube[:, 1, :, :],
                )
                loss_tube_ett_normal = self.dice_loss(
                    selected_pred_tube[:, 3, :, :],
                    selected_real_tube[:, 2, :, :],
                )
                loss_tube_ngt_abnormal = self.dice_loss(
                    selected_pred_tube[:, 4, :, :],
                    selected_real_tube[:, 3, :, :],
                )
                loss_tube_ngt_borderline = self.dice_loss(
                    selected_pred_tube[:, 5, :, :],
                    selected_real_tube[:, 4, :, :],
                )
                loss_tube_ngt_incomp_image = self.dice_loss(
                    selected_pred_tube[:, 6, :, :],
                    selected_real_tube[:, 5, :, :],
                )
                loss_tube_ngt_normal = self.dice_loss(
                    selected_pred_tube[:, 7, :, :],
                    selected_real_tube[:, 6, :, :],
                )
                loss_tube_cvc_abnormal = self.dice_loss(
                    selected_pred_tube[:, 8, :, :],
                    selected_real_tube[:, 7, :, :],
                )
                loss_tube_cvc_borderline = self.dice_loss(
                    selected_pred_tube[:, 9, :, :],
                    selected_real_tube[:, 8, :, :],
                )
                loss_tube_cvc_normal = self.dice_loss(
                    selected_pred_tube[:, 10, :, :],
                    selected_real_tube[:, 9, :, :],
                )
                loss_tube_swgc_present = self.dice_loss(
                    selected_pred_tube[:, 11, :, :],
                    selected_real_tube[:, 10, :, :],
                )
                loss_tube = (
                    loss_tube_ett_abnormal * 2
                    + loss_tube_ett_borderline * 2
                    + loss_tube_ett_normal
                    + loss_tube_ngt_abnormal * 2
                    + loss_tube_ngt_borderline
                    + loss_tube_ngt_incomp_image
                    + loss_tube_ngt_normal
                    + loss_tube_cvc_abnormal
                    + loss_tube_cvc_borderline
                    + loss_tube_cvc_normal
                    + loss_tube_swgc_present * 2
                ) / 15
            else:
                loss_tube = self.dice_loss(
                    selected_pred_tube[:, 1, :, :], selected_real_tube
                )
        else:
            if self.multi_channel:
                loss_tube_ett_abnormal = 0
                loss_tube_ett_borderline = 0
                loss_tube_ett_normal = 0
                loss_tube_ngt_abnormal = 0
                loss_tube_ngt_borderline = 0
                loss_tube_ngt_incomp_image = 0
                loss_tube_ngt_normal = 0
                loss_tube_cvc_abnormal = 0
                loss_tube_cvc_borderline = 0
                loss_tube_cvc_normal = 0
                loss_tube_swgc_present = 0
            loss_tube = 0

        # Compute BCE final
        if self.label_smoothing_coef > 0 and model.training:
            loss_target = label_smoothing(
                target.float(), smoothing=self.label_smoothing_coef
            )
        else:
            loss_target = target.float()

        loss_bce = self.bce_loss(pred_label, loss_target)

        # Compute BCE hidden
        if self.double_mlp:
            if self.label_smoothing_coef > 0 and model.training:
                loss_target_hidden = label_smoothing(
                    target_hidden.float(), smoothing=self.label_smoothing_coef
                )
            else:
                loss_target_hidden = target_hidden.float()

            loss_hidden_bce = self.bce_loss(
                pred_label_hidden, loss_target_hidden
            )

        if lr is not None and lr < self.lr_thresh:
            loss = loss_bce
        else:
            loss = (
                loss_lung * self.lung_coef
                + loss_tube * self.tube_coef
                + loss_bce * self.bce_coef
            )
        if self.double_mlp:
            loss = loss + loss_hidden_bce * self.hidden_bce_coef

        if self.multi_channel:
            loss_dict = dict(
                loss=loss,
                dice_lung=loss_lung,
                dice_tube=loss_tube,
                bce=loss_bce,
                loss_tube_ett_abnormal=loss_tube_ett_abnormal,
                loss_tube_ett_borderline=loss_tube_ett_borderline,
                loss_tube_ett_normal=loss_tube_ett_normal,
                loss_tube_ngt_abnormal=loss_tube_ngt_abnormal,
                loss_tube_ngt_borderline=loss_tube_ngt_borderline,
                loss_tube_ngt_incomp_image=loss_tube_ngt_incomp_image,
                loss_tube_ngt_normal=loss_tube_ngt_normal,
                loss_tube_cvc_abnormal=loss_tube_cvc_abnormal,
                loss_tube_cvc_borderline=loss_tube_cvc_borderline,
                loss_tube_cvc_normal=loss_tube_cvc_normal,
                loss_tube_swgc_present=loss_tube_swgc_present,
            )
        else:
            loss_dict = dict(
                loss=loss,
                dice_lung=loss_lung,
                dice_tube=loss_tube,
                bce=loss_bce,
            )

        if self.double_mlp:
            loss_dict["loss_bce_hidden"] = loss_hidden_bce

        if self.multi_channel:
            mask = torch.cat(
                [torch.unsqueeze(mask_lung, dim=1), mask_tube], dim=1
            )
        else:
            mask = torch.stack([mask_lung, mask_tube], dim=1)

        return loss_dict, mask, pred_mask, target, pred_label
