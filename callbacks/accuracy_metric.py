from typing import Callable, Dict, Any

import torch
import numpy as np

from scipy.special import expit
from catalyst.dl import Callback, CallbackOrder


class MetricWrapper(Callback):
    def __init__(
        self,
        metric_func: Callable,
        metric_name: str,
        aggregation_policy_pred: str = "raw",
        aggregation_policy_target: str = "raw",
        metric_kwargs: Dict[str, str] = {},
    ):
        super().__init__(CallbackOrder.Metric)

        if aggregation_policy_pred not in ["raw", "argmax", "many_hot"]:
            raise ValueError("Invalid aggregation_policy_pred")
        if aggregation_policy_target not in ["raw", "argmax", "many_hot"]:
            raise ValueError("Invalid aggregation_policy_target")

        self.metric_func = metric_func
        self.metric_name = metric_name
        self.aggregation_policy_pred = aggregation_policy_pred
        self.aggregation_policy_target = aggregation_policy_target
        self.metric_kwargs = metric_kwargs

        self.running_preds = []
        self.running_targets = []

    def on_batch_end(self, state):
        y_hat = state.output["logits"].detach().cpu().numpy()
        y = state.input["targets"].detach().cpu().numpy()

        self.running_preds.append(y_hat)
        self.running_targets.append(y)

    def _compute_metric(self, y_true, y_pred):
        if self.aggregation_policy_pred == "raw":
            pass
        elif self.aggregation_policy_pred == "argmax":
            y_pred = y_pred.argmax(-1)
        elif self.aggregation_policy_pred == "many_hot":
            y_pred = (expit(y_pred) > 0.5).astype(int)

        if self.aggregation_policy_target == "raw":
            pass
        elif self.aggregation_policy_target == "argmax":
            y_true = y_true.argmax(-1)
        elif self.aggregation_policy_target == "many_hot":
            y_true = (y_true > 0.5).astype(int)

        return self.metric_func(y_true, y_pred, **self.metric_kwargs)

    def on_loader_end(self, state):
        y_true = np.concatenate(self.running_targets)
        y_pred = np.concatenate(self.running_preds)

        score = self._compute_metric(y_true, y_pred)

        state.loader_metrics[self.metric_name] = score

        self.running_preds = []
        self.running_targets = []
