from os.path import join as pjoin
from copy import deepcopy

import torch
import numpy as np

from catalyst.dl import Callback, CallbackOrder
from sklearn.metrics import roc_auc_score


class SWACallback(Callback):
    def __init__(
        self,
        num_of_swa_models: int,
        maximize: bool,
        logging_metric: str,
        verbose: bool,
    ):
        super().__init__(CallbackOrder.External)
        self.model_checkpoints = [None] * num_of_swa_models
        self.scores = (
            [-np.inf] * num_of_swa_models
            if maximize
            else [np.inf] * num_of_swa_models
        )
        self.best_epochs = [None] * num_of_swa_models

        self.maximize = maximize
        self.logging_metric = logging_metric
        self.verbose = verbose

    def on_stage_end(self, state):
        path_name = pjoin(
            state.logdir, "checkpoints", f"swa_models_{self.logging_metric}.pt"
        )
        save_dict = {
            i: (s, m, e)
            for i, (s, m, e) in enumerate(
                zip(self.scores, self.model_checkpoints, self.best_epochs)
            )
        }
        torch.save(save_dict, path_name)

    def _put_state_dict_on_cpu(self, sd):
        sd_copy = deepcopy(sd)
        for k, v in sd_copy.items():
            sd_copy[k] = v.cpu()
        return sd_copy

    def on_epoch_end(self, state):
        epoch_metric = state.loader_metrics[self.logging_metric]
        epoch_n = state.epoch

        if self.maximize:
            if np.min(self.scores) < epoch_metric:
                min_value_index = np.argmin(self.scores)
                self.scores[min_value_index] = epoch_metric
                self.model_checkpoints[
                    min_value_index
                ] = self._put_state_dict_on_cpu(state.model.state_dict())
                self.best_epochs[min_value_index] = epoch_n
        else:
            if np.max(self.scores) > epoch_metric:
                max_value_index = np.argmax(self.scores)
                self.scores[max_value_index] = epoch_metric
                self.model_checkpoints[
                    max_value_index
                ] = self._put_state_dict_on_cpu(state.model.state_dict())
                self.best_epochs[max_value_index] = epoch_n

        if self.verbose:
            print(
                "Best models scores by {} : {}".format(
                    self.logging_metric, self.scores
                )
            )
