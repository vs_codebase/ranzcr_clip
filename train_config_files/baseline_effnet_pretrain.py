import sys

sys.path.append("../")

import os

from glob import glob

import torch
import albumentations as albu
import torchvision.transforms as T
import timm

from catalyst.callbacks import (
    OptimizerCallback,
    SchedulerCallback,
    EarlyStoppingCallback,
)

# from transformers import get_cosine_schedule_with_warmup
from sklearn.metrics import roc_auc_score

from train_functions import catalyst_training
from datasets import RANZCRDataset
from models import CNNPreatrainModel
from utils.constants import CLASSES_CHESTX
from losses import StrongBCE
from callbacks import SWACallback, MetricWrapper


B_S = 16
RES = 640
ROOT_PATH = f"/data/additional_data/chestx/all_images_{RES}_{RES}/"

CONFIG = {
    "seed": 1243,
    "df_path": "/data/additional_data/chestx/train.csv",
    "split_path": "/data/additional_data/chestx/cv_split_original_train.npy",
    "exp_name": f"timm_efficientnet_b5_pretrain_chestx_{B_S}bs_{RES}res_qubvelaugs_rotaugs_ls005_shedchanged_nol1_trainval",
    "files_to_save": (
        glob("callbacks/*.py")
        + glob("datasets/*.py")
        + glob("losses/*.py")
        + glob("models/*.py")
        + glob("scripts/*.py")
        + glob("train_functions/*.py")
        + glob("utils/*.py")
        + [__file__]
        + ["main_train.py"]
    ),
    "folds": [0],
    "train_function": catalyst_training,
    "train_function_args": {
        "train_dataset_class": RANZCRDataset,
        "train_dataset_config": {
            "root": ROOT_PATH,
            "path_col": "Image Index",
            "ext": ".jpeg",
            "target_col": CLASSES_CHESTX[1:],
            "augmentations": albu.Compose(
                [
                    # TTA augs
                    albu.Transpose(p=0.5),
                    albu.HorizontalFlip(p=0.5),
                    albu.VerticalFlip(p=0.5),
                    # Special augs
                    albu.ShiftScaleRotate(
                        scale_limit=0.5,
                        rotate_limit=0,
                        shift_limit=0.1,
                        p=0.75,
                        border_mode=0,
                    ),
                    # Some strange augs
                    albu.IAAAdditiveGaussianNoise(p=0.2),
                    albu.IAAPerspective(p=0.5),
                    # SOTA augs
                    albu.Cutout(
                        p=0.5, max_h_size=32, max_w_size=32, num_holes=8
                    ),
                ]
            ),
            "transforms": T.ToTensor(),
            "do_fmix": False,
            "do_mixup": False,
            "do_cutmix": False,
        },
        "val_dataset_class": RANZCRDataset,
        "val_dataset_config": {
            "root": ROOT_PATH,
            "path_col": "Image Index",
            "ext": ".jpeg",
            "target_col": CLASSES_CHESTX[1:],
            "transforms": T.ToTensor(),
            "do_fmix": False,
            "do_mixup": False,
            "do_cutmix": False,
        },
        "train_dataloader_config": {
            "batch_size": B_S,
            "shuffle": True,
            "drop_last": True,
            "num_workers": os.cpu_count() // 2,
        },
        "val_dataloader_config": {
            "batch_size": B_S,
            "shuffle": False,
            "drop_last": False,
            "num_workers": os.cpu_count() // 2,
        },
        "nn_model_class": CNNPreatrainModel,
        "nn_model_config": {
            "classifiier_config": {
                "classifier_type": "drop_linear",
                "classes_num": 15,
                "hidden_dims": 1024,
                "second_dropout_rate": 0.2,
            },
            "encoder_config": {
                "in_channels": 1,
                "encoder_name": "timm-efficientnet-b5",
                "encoder_weights": "noisy-student",
                "classes": 12,
                "activation": "sigmoid",
                "aux_params": dict(
                    pooling="avg",  # one of 'avg', 'max'
                    dropout=None,  # dropout ratio, default is None
                    classes=4,  # define number of output labels
                ),
            },
            "encoder_type": "timm-efficientnet-b5_unet",
        },
        "optimizer_init": lambda model: torch.optim.Adam(
            model.parameters(), lr=0.0005
        ),
        "scheduler_init": lambda optimizer, train_loader_len: torch.optim.lr_scheduler.ReduceLROnPlateau(
            optimizer, patience=3, factor=0.5, min_lr=1e-7, mode="max"
        ),
        "criterion": StrongBCE(
            use_label_smotthing=True, label_smoothing_coef=0.05
        ),
        "n_epochs": 200,
        "distributed": True,
        "catalyst_callbacks": lambda: [
            OptimizerCallback(
                metric_key="loss",
                accumulation_steps=1,
                use_fast_zero_grad=True,
            ),
            MetricWrapper(
                metric_func=roc_auc_score,
                metric_name="roc_auc_score",
                aggregation_policy_pred="raw",
                aggregation_policy_target="many_hot",
            ),
            SWACallback(
                num_of_swa_models=3,
                maximize=True,
                logging_metric="roc_auc_score",
                verbose=True,
            ),
            EarlyStoppingCallback(
                patience=7, metric="roc_auc_score", minimize=False
            ),
            SchedulerCallback(mode="epoch"),
        ],
        "main_metric": "roc_auc_score",
        "minimize_metric": False,
        "use_mixed_precision": "amp",
    },
}
