import sys

sys.path.append("../")

import os

from glob import glob

import torch
import albumentations as albu
import torchvision.transforms as T
import timm

from catalyst.callbacks import (
    OptimizerCallback,
    SchedulerCallback,
    EarlyStoppingCallback,
)

# from transformers import get_cosine_schedule_with_warmup
from sklearn.metrics import roc_auc_score
from catalyst.contrib.callbacks.draw_masks_callback import DrawMasksCallback

from train_functions import catalyst_training_mixed
from datasets import RANZCRMixedDataset
from models import CNNPreatrainModelSeg
from utils.constants import CLASSES_CHESTX, CLASSES
from losses import MultiTaskLoss
from callbacks import SWACallback, MetricWrapper


B_S = 24
RES = 640
ROOT_PATH = f"/data/train_{RES}_{RES}/"
ROOT_IMAGE = f"/data/train_{RES}_{RES}/"
ROOT_LUNG = (
    f"/data/additional_data/train_lung_masks/train_lung_masks_{RES}_{RES}/"
)
ROOT_TUBE = f"/data/tube_masks_{RES}_{RES}/"

CONFIG = {
    "seed": 1243,
    "df_path": "/data/enlarged_train.csv",
    "split_path": "/data/naive_cv_split.npy",
    "exp_name": f"timm_efficientnet_b5_unet_{B_S}bs_{RES}res_qubvelaugs_rotaugs_ls005_shedchanged_mypretrain_difflrs_segbranch_113coefs_1e4noseg_bh",
    "files_to_save": (
        glob("callbacks/*.py")
        + glob("datasets/*.py")
        + glob("losses/*.py")
        + glob("models/*.py")
        + glob("scripts/*.py")
        + glob("train_functions/*.py")
        + glob("utils/*.py")
        + [__file__]
        + ["main_train.py"]
    ),
    "folds": [0, 1, 2, 3, 4],
    "train_function": catalyst_training_mixed,
    "train_function_args": {
        "train_dataset_class": RANZCRMixedDataset,
        "train_dataset_config": {
            "root_img": ROOT_IMAGE,
            "root_tube": ROOT_TUBE,
            "root_lung": ROOT_LUNG,
            "path_col": "StudyInstanceUID",
            "ext": ".jpeg",
            "target_col": CLASSES + CLASSES_CHESTX,
            "pixel_thresh": 100,
            "multi_channel": False,
            "mask_to_memmory": False,
            "hidden_target": False,
            "augmentations": albu.Compose(
                [
                    # TTA augs
                    albu.Transpose(p=0.5),
                    albu.HorizontalFlip(p=0.5),
                    albu.VerticalFlip(p=0.5),
                    albu.ShiftScaleRotate(
                        scale_limit=0,
                        rotate_limit=(15, 16),
                        shift_limit=0,
                        p=0.5,
                        border_mode=0,
                    ),
                    albu.ShiftScaleRotate(
                        scale_limit=0,
                        rotate_limit=(-16, -15),
                        shift_limit=0,
                        p=0.5,
                        border_mode=0,
                    ),
                    # Special augs
                    albu.ShiftScaleRotate(
                        scale_limit=0.5,
                        rotate_limit=0,
                        shift_limit=0.1,
                        p=0.75,
                        border_mode=0,
                    ),
                    # Some strange augs
                    albu.IAAAdditiveGaussianNoise(p=0.2),
                    albu.IAAPerspective(p=0.5),
                    # Blur augs
                    albu.OneOf(
                        [
                            albu.Blur(blur_limit=3, p=1),
                            albu.MotionBlur(blur_limit=3, p=1),
                        ],
                        p=0.9,
                    ),
                    # SOTA augs
                    albu.Cutout(
                        p=0.5, max_h_size=64, max_w_size=64, num_holes=4
                    ),
                ]
            ),
            "transforms": T.ToTensor(),
        },
        "val_dataset_class": RANZCRMixedDataset,
        "val_dataset_config": {
            "root_img": ROOT_IMAGE,
            "root_tube": ROOT_TUBE,
            "root_lung": ROOT_LUNG,
            "path_col": "StudyInstanceUID",
            "ext": ".jpeg",
            "pixel_thresh": 100,
            "target_col": CLASSES + CLASSES_CHESTX,
            "multi_channel": False,
            "mask_to_memmory": False,
            "hidden_target": False,
            "transforms": T.ToTensor(),
        },
        "train_dataloader_config": {
            "batch_size": B_S,
            "shuffle": True,
            "drop_last": True,
            "num_workers": os.cpu_count() // 2,
        },
        "val_dataloader_config": {
            "batch_size": B_S,
            "shuffle": False,
            "drop_last": False,
            "num_workers": os.cpu_count() // 2,
        },
        "nn_model_class": CNNPreatrainModelSeg,
        "nn_model_config": {
            "classifiier_config": {
                "classes_num": 16,
                "main_classes_num": 11,
                "hidden_dims": 1024,
                "first_dropout_rate": 0.3,
                "second_dropout_rate": 0.2,
            },
            "encoder_config": {
                "in_channels": 1,
                "encoder_name": "timm-efficientnet-b5",
                "encoder_weights": None,
                "classes": 12,
                "activation": "sigmoid",
                "aux_params": dict(
                    pooling="avg",  # one of 'avg', 'max'
                    dropout=None,  # dropout ratio, default is None
                    classes=4,  # define number of output labels
                ),
            },
            "encoder_type": "timm-efficientnet-b5_unet",
            "path_to_chkp": "pretrained/timm_efficientnet_b5_pretrain_chestx_32bs_640res_qubvelaugs_rotaugs_ls005_shedchanged_swa_loss_fold_0.pt",
        },
        "optimizer_init": lambda model: torch.optim.Adam(
            [
                {
                    "params": model.module.encoder.encoder.parameters(),
                    "lr": 0.0002,
                },
                {
                    "params": model.module.encoder.segmentation_head.parameters(),
                    "lr": 0.001,
                },
                {
                    "params": model.module.encoder.classification_head.parameters(),
                    "lr": 0.001,
                },
                {
                    "params": model.module.encoder.decoder.parameters(),
                    "lr": 0.001,
                },
                {"params": model.module.classifier.parameters(), "lr": 0.0002},
                {
                    "params": model.module.main_classifier.parameters(),
                    "lr": 0.001,
                },
            ]
        ),
        "scheduler_init": lambda optimizer, train_loader_len: torch.optim.lr_scheduler.ReduceLROnPlateau(
            optimizer, patience=3, factor=0.5, min_lr=1e-7, mode="max"
        ),
        "criterion": MultiTaskLoss(
            use_label_smotthing=True,
            label_smoothing_coef=0.05,
            lung_coef=1.0,
            tube_coef=1.0,
            bce_coef=3.0,
            additional_bce_coef=0.0,
            additional_l1_coef=0.0,
            lr_thresh=0.0001,
        ),
        "n_epochs": 200,
        "distributed": True,
        "catalyst_callbacks": lambda: [
            OptimizerCallback(
                metric_key="loss",
                accumulation_steps=1,
                use_fast_zero_grad=True,
            ),
            MetricWrapper(
                metric_func=roc_auc_score,
                metric_name="roc_auc_score",
                aggregation_policy_pred="raw",
                aggregation_policy_target="many_hot",
            ),
            SWACallback(
                num_of_swa_models=3,
                maximize=True,
                logging_metric="roc_auc_score",
                verbose=True,
            ),
            DrawMasksCallback(
                output_key="pred_mask",
                input_mask_key="mask",
                activation="none",
                summary_step=200,
            ),
            EarlyStoppingCallback(
                patience=7, metric="roc_auc_score", minimize=False
            ),
            SchedulerCallback(mode="epoch"),
        ],
        "main_metric": "roc_auc_score",
        "minimize_metric": False,
        "use_mixed_precision": "amp",
    },
}
