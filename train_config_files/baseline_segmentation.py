import sys

sys.path.append("../")

import os

from glob import glob

import torch
import albumentations as albu
import torchvision.transforms as T
import timm
import segmentation_models_pytorch as smp

from catalyst.callbacks import (
    OptimizerCallback,
    SchedulerCallback,
    EarlyStoppingCallback,
)
from catalyst.contrib.callbacks.draw_masks_callback import DrawMasksCallback


from sklearn.metrics import roc_auc_score

from train_functions import catalyst_training_segmentation
from datasets import RANZCRSegementationDataset
from losses import LungTubeDice
from callbacks import SWACallback, MetricWrapper


B_S = 20
RES = 512
ROOT_IMAGE = f"/data/train_{RES}_{RES}/"
ROOT_LUNG = (
    f"/data/additional_data/train_lung_masks/train_lung_masks_{RES}_{RES}/"
)
ROOT_TUBE = f"/data/tube_masks_{RES}_{RES}/"

CONFIG = {
    "seed": 1243,
    "df_path": "/data/train_only_anot.csv",
    "split_path": "/data/naive_cv_split_only_anot.npy",
    "exp_name": f"fpn_se_resnext50_32x4d_imagenet_{B_S}bs_{RES}res_qubvelaugs_shedchanged",
    "files_to_save": (
        glob("callbacks/*.py")
        + glob("datasets/*.py")
        + glob("losses/*.py")
        + glob("models/*.py")
        + glob("scripts/*.py")
        + glob("train_functions/*.py")
        + glob("utils/*.py")
        + [__file__]
        + ["main_train.py"]
    ),
    "folds": [1, 2, 3, 4],
    "train_function": catalyst_training_segmentation,
    "train_function_args": {
        "train_dataset_class": RANZCRSegementationDataset,
        "train_dataset_config": {
            "root_img": ROOT_IMAGE,
            "root_tube": ROOT_TUBE,
            "root_lung": ROOT_LUNG,
            "path_col": "StudyInstanceUID",
            "ext": ".jpeg",
            "pixel_thresh": 100,
            "augmentations": albu.Compose(
                [
                    albu.HorizontalFlip(p=0.5),
                    albu.HorizontalFlip(p=0.5),
                    albu.Transpose(p=0.5),
                    albu.ShiftScaleRotate(
                        scale_limit=0.5,
                        rotate_limit=0,
                        shift_limit=0.1,
                        p=1,
                        border_mode=0,
                    ),
                    albu.IAAAdditiveGaussianNoise(p=0.2),
                    albu.IAAPerspective(p=0.5),
                    albu.OneOf(
                        [
                            albu.CLAHE(p=1),
                            albu.RandomBrightness(p=1),
                            albu.RandomGamma(p=1),
                        ],
                        p=0.9,
                    ),
                    albu.OneOf(
                        [
                            albu.IAASharpen(p=1),
                            albu.Blur(blur_limit=3, p=1),
                            albu.MotionBlur(blur_limit=3, p=1),
                        ],
                        p=0.9,
                    ),
                ]
            ),
            "transforms": T.ToTensor(),
        },
        "val_dataset_class": RANZCRSegementationDataset,
        "val_dataset_config": {
            "root_img": ROOT_IMAGE,
            "root_tube": ROOT_TUBE,
            "root_lung": ROOT_LUNG,
            "path_col": "StudyInstanceUID",
            "ext": ".jpeg",
            "pixel_thresh": 100,
            "transforms": T.ToTensor(),
        },
        "train_dataloader_config": {
            "batch_size": B_S,
            "shuffle": True,
            "drop_last": True,
            "num_workers": os.cpu_count() // 2,
        },
        "val_dataloader_config": {
            "batch_size": B_S,
            "shuffle": False,
            "drop_last": False,
            "num_workers": os.cpu_count() // 2,
        },
        "nn_model_class": smp.FPN,
        "nn_model_config": {
            "in_channels": 1,
            "encoder_name": "se_resnext50_32x4d",
            "encoder_weights": "imagenet",
            "classes": 2,
            "activation": "sigmoid",
        },
        "optimizer_init": lambda model: torch.optim.Adam(
            [
                {"params": model.module.encoder.parameters(), "lr": 0.00025},
                {"params": model.module.decoder.parameters(), "lr": 0.001},
                {
                    "params": model.module.segmentation_head.parameters(),
                    "lr": 0.001,
                },
            ]
        ),
        "scheduler_init": lambda optimizer, train_loader_len: torch.optim.lr_scheduler.ReduceLROnPlateau(
            optimizer, patience=3, factor=0.5, min_lr=1e-7, mode="min"
        ),
        "criterion": LungTubeDice(lung_coef=1.0, tube_coef=1.0),
        "n_epochs": 200,
        "distributed": True,
        "catalyst_callbacks": lambda: [
            OptimizerCallback(metric_key="loss", accumulation_steps=1),
            SWACallback(
                num_of_swa_models=3,
                maximize=False,
                logging_metric="loss",
                verbose=True,
            ),
            EarlyStoppingCallback(patience=7, metric="loss", minimize=True),
            SchedulerCallback(mode="epoch"),
            DrawMasksCallback(
                output_key="pred_mask",
                input_mask_key="mask",
                activation="none",
                summary_step=200,
            ),
        ],
        "main_metric": "loss",
        "minimize_metric": True,
        "use_mixed_precision": False,
    },
}
