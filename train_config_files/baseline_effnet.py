import sys

sys.path.append("../")

import os

from glob import glob

import torch
import albumentations as albu
import torchvision.transforms as T
import timm

from catalyst.callbacks import (
    OptimizerCallback,
    SchedulerCallback,
    EarlyStoppingCallback,
)

# from transformers import get_cosine_schedule_with_warmup
from sklearn.metrics import roc_auc_score

from train_functions import catalyst_training
from datasets import RANZCRDataset
from models import CNNModel
from losses import StrongBCE, StrongCatalystFocal
from callbacks import SWACallback, MetricWrapper


B_S = 40
RES = 640
ROOT_PATH = f"/data/train_{RES}_{RES}/"

CONFIG = {
    "seed": 1243,
    "df_path": "/data/train.csv",
    "split_path": "/data/naive_cv_split.npy",
    "exp_name": f"resnet200d_{B_S}bs_{RES}res_qubvelaugs_ls005_shedchanged_startpoint_difflrs",
    "files_to_save": (
        glob("callbacks/*.py")
        + glob("datasets/*.py")
        + glob("losses/*.py")
        + glob("models/*.py")
        + glob("scripts/*.py")
        + glob("train_functions/*.py")
        + glob("utils/*.py")
        + [__file__]
        + ["main_train.py"]
    ),
    "folds": [0, 1, 2, 3, 4],
    "train_function": catalyst_training,
    "train_function_args": {
        "train_dataset_class": RANZCRDataset,
        "train_dataset_config": {
            "root": ROOT_PATH,
            "path_col": "StudyInstanceUID",
            "ext": ".jpeg",
            # "mask_root": "/data/seg_preds/fpn_se_resnext50_32x4d_imagenet_20bs_512res_qubvelaugs_shedchanged_swa_fold0_notta/train_not_anot",
            "augmentations": albu.Compose(
                [
                    albu.Transpose(p=0.5),
                    albu.HorizontalFlip(p=0.5),
                    albu.VerticalFlip(p=0.5),
                    albu.ShiftScaleRotate(
                        scale_limit=0.5,
                        rotate_limit=0,
                        shift_limit=0.1,
                        p=0.75,
                        border_mode=0,
                    ),
                    albu.IAAAdditiveGaussianNoise(p=0.2),
                    albu.IAAPerspective(p=0.5),
                    albu.OneOf(
                        [
                            albu.Blur(blur_limit=3, p=1),
                            albu.MotionBlur(blur_limit=3, p=1),
                        ],
                        p=0.9,
                    ),
                    albu.Cutout(p=0.5, max_h_size=32, max_w_size=32),
                ]
            ),
            "transforms": T.ToTensor(),
            "do_fmix": False,
            "do_mixup": False,
            "mixup_params": {"prob": 0.5, "alpha": 0.3},
            "do_cutmix": False,
            "cutmix_params": {"prob": 0.5, "alpha": 0.3},
        },
        "val_dataset_class": RANZCRDataset,
        "val_dataset_config": {
            "root": ROOT_PATH,
            "path_col": "StudyInstanceUID",
            "ext": ".jpeg",
            # "mask_root": "/data/seg_preds/fpn_se_resnext50_32x4d_imagenet_20bs_512res_qubvelaugs_shedchanged_swa_fold0_notta/train_not_anot",
            "transforms": T.ToTensor(),
            "do_fmix": False,
        },
        "train_dataloader_config": {
            "batch_size": B_S,
            "shuffle": True,
            "drop_last": True,
            "num_workers": os.cpu_count() // 2,
        },
        "val_dataloader_config": {
            "batch_size": B_S,
            "shuffle": False,
            "drop_last": False,
            "num_workers": os.cpu_count() // 2,
        },
        "nn_model_class": CNNModel,
        "nn_model_config": {
            "classifiier_config": {
                "classifier_type": "elu",
                "classes_num": 11,
                "hidden_dims": 1024,
                "second_dropout_rate": 0.2,
                "first_dropout_rate": 0.3,
            },
            "encoder_type": "resnet200d",
            "use_taylorsoftmax": False,
            "one_channel": True,
            "path_to_chkp": "/data/additional_data/startingpoints/resnet200d_320_chestx.pth",
        },
        "optimizer_init": lambda model: torch.optim.Adam(
            [
                {"params": model.module.encoder.parameters(), "lr": 0.00025},
                {"params": model.module.classifier.parameters(), "lr": 0.001},
            ]
        ),
        # "optimizer_init": lambda model: torch.optim.Adam(
        #     model.parameters(), lr=0.0001
        # ),
        "scheduler_init": lambda optimizer, train_loader_len: torch.optim.lr_scheduler.ReduceLROnPlateau(
            optimizer, patience=3, factor=0.5, min_lr=1e-7, mode="max"
        ),
        "criterion": StrongBCE(
            use_label_smotthing=True, label_smoothing_coef=0.05
        ),
        "n_epochs": 200,
        "distributed": True,
        "catalyst_callbacks": lambda: [
            OptimizerCallback(
                metric_key="loss",
                accumulation_steps=1,
                use_fast_zero_grad=True,
            ),
            MetricWrapper(
                metric_func=roc_auc_score,
                metric_name="roc_auc_score",
                aggregation_policy_pred="raw",
                aggregation_policy_target="many_hot",
            ),
            SWACallback(
                num_of_swa_models=3,
                maximize=True,
                logging_metric="roc_auc_score",
                verbose=True,
            ),
            EarlyStoppingCallback(
                patience=7, metric="roc_auc_score", minimize=False
            ),
            SchedulerCallback(mode="epoch"),
        ],
        "main_metric": "roc_auc_score",
        "minimize_metric": False,
        "use_mixed_precision": "amp",
    },
}
