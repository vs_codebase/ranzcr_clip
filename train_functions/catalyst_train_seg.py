import shutil
import os
import warnings

from typing import Callable, Optional, Union
from os.path import join as pjoin

import pandas as pd
import numpy as np
import torch

from catalyst.dl import Runner


class CustomRunner(Runner):
    def _handle_batch(self, batch):

        loss_dict, mask, pred_mask = self.criterion(batch, self.model)

        self.batch_metrics.update(loss_dict)
        self.input = dict(mask=mask)
        self.output = dict(pred_mask=pred_mask)


def set_device():
    if torch.cuda.is_available():
        device = "cuda"
    else:
        device = "cpu"
    print(f"Training Device : {device}")
    return device


def set_training_precision(use_mixed_precision: Union[str, bool]):
    if not use_mixed_precision:
        return None
    elif use_mixed_precision == "amp":
        return dict(amp=True)
    elif use_mixed_precision == "apex":
        return dict(apex=True, opt_level="O1")
    else:
        raise RuntimeError(
            f"{use_mixed_precision} is inccorect"
            " value for `use_mixed_precision`"
        )


def catalyst_training(
    train_df: pd.DataFrame,
    val_df: pd.DataFrame,
    exp_name: str,
    train_dataset_class: torch.utils.data.Dataset,
    val_dataset_class: torch.utils.data.Dataset,
    train_dataset_config: dict,
    val_dataset_config: dict,
    train_dataloader_config: dict,
    val_dataloader_config: dict,
    nn_model_class: torch.nn.Module,
    nn_model_config: dict,
    optimizer_init: Callable,
    scheduler_init: Callable,
    criterion: torch.nn.Module,
    n_epochs: int,
    distributed: bool,
    catalyst_callbacks: Callable,
    main_metric: str,
    minimize_metric: bool,
    use_mixed_precision: Union[str, bool] = False,
):
    device = set_device()

    train_dataset = train_dataset_class(df=train_df, **train_dataset_config)
    val_dataset = val_dataset_class(df=val_df, **val_dataset_config)

    loaders = {
        "train": torch.utils.data.DataLoader(
            train_dataset, **train_dataloader_config
        ),
        "valid": torch.utils.data.DataLoader(
            val_dataset, **val_dataloader_config
        ),
    }

    model = nn_model_class(**nn_model_config).to(device)

    print(model)

    if distributed:
        model = torch.nn.DataParallel(model)

    optimizer = optimizer_init(model)
    scheduler = scheduler_init(optimizer, len(loaders["train"]))

    runner = CustomRunner()

    if os.path.exists(exp_name):
        raise RuntimeError(f"Logdir {exp_name} exists!")

    runner.train(
        model=model,
        optimizer=optimizer,
        criterion=criterion,
        scheduler=scheduler,
        loaders=loaders,
        logdir=exp_name,
        num_epochs=n_epochs,
        verbose=True,
        load_best_on_end=True,
        main_metric=main_metric,
        minimize_metric=minimize_metric,
        callbacks=catalyst_callbacks(),  # We need to call this to make unique objects for each fold
        fp16=set_training_precision(use_mixed_precision),
    )

    best_chkp = torch.load(
        pjoin(exp_name, "checkpoints/best_full.pth"), map_location="cpu"
    )

    return best_chkp
