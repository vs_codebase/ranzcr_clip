from .catalyst_train import catalyst_training
from .catalyst_train_seg import (
    catalyst_training as catalyst_training_segmentation,
)
from .catalyst_train_mixed import catalyst_training as catalyst_training_mixed
