import argparse
import os
import random

from typing import List
from os.path import join as pjoin
from collections import Counter, defaultdict

import pandas as pd
import numpy as np

from sklearn.preprocessing import LabelEncoder

RS = 42
SMALL_CLASS_REPRES = 5


def collect_args():

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument("df_path", type=str, help="Path to train dataframe")
    parser.add_argument(
        "--save_path",
        type=str,
        help="Path to .npy file to save split",
        default="",
    )
    parser.add_argument(
        "--n_folds", type=int, help="Number of CV folds", default=5
    )

    args = parser.parse_args()

    return args


def stratified_group_k_fold(X, y, groups, k, seed=RS):
    labels_num = np.max(y) + 1
    y_counts_per_group = defaultdict(lambda: np.zeros(labels_num))
    y_distr = Counter()
    for label, g in zip(y, groups):
        y_counts_per_group[g][label] += 1
        y_distr[label] += 1

    y_counts_per_fold = defaultdict(lambda: np.zeros(labels_num))
    groups_per_fold = defaultdict(set)

    def eval_y_counts_per_fold(y_counts, fold):
        y_counts_per_fold[fold] += y_counts
        std_per_label = []
        for label in range(labels_num):
            label_std = np.std(
                [
                    y_counts_per_fold[i][label] / y_distr[label]
                    for i in range(k)
                ]
            )
            std_per_label.append(label_std)
        y_counts_per_fold[fold] -= y_counts
        return np.mean(std_per_label)

    groups_and_y_counts = list(y_counts_per_group.items())
    random.Random(seed).shuffle(groups_and_y_counts)

    for g, y_counts in sorted(
        groups_and_y_counts, key=lambda x: -np.std(x[1])
    ):
        best_fold = None
        min_eval = None
        for i in range(k):
            fold_eval = eval_y_counts_per_fold(y_counts, i)
            if min_eval is None or fold_eval < min_eval:
                min_eval = fold_eval
                best_fold = i
        y_counts_per_fold[best_fold] += y_counts
        groups_per_fold[best_fold].add(g)

    all_groups = set(groups)
    for i in range(k):
        train_groups = all_groups - groups_per_fold[i]
        test_groups = groups_per_fold[i]

        train_indices = [i for i, g in enumerate(groups) if g in train_groups]
        test_indices = [i for i, g in enumerate(groups) if g in test_groups]

        yield np.array(train_indices), np.array(test_indices)


def create_class_col(input_df: pd.DataFrame):
    classes = input_df.columns.tolist()[1:-1]

    input_df["merged_classes"] = input_df[classes].apply(
        lambda x: ".".join([str(el) for el in x]), axis=1
    )

    small_clases = (
        input_df["merged_classes"]
        .value_counts()[
            input_df["merged_classes"].value_counts() < SMALL_CLASS_REPRES
        ]
        .index
    )

    input_df.loc[
        input_df["merged_classes"].isin(small_clases), "merged_classes"
    ] = "small_repr_class"

    print("Merged value counts")
    print(input_df["merged_classes"].value_counts())

    return input_df


if __name__ == "__main__":

    args = collect_args()

    print(f"Received args: {args}")

    if args.save_path == "":
        save_path = pjoin(os.path.dirname(args.df_path), "cv_split.npy")
    else:
        save_path = args.save_path

    df = pd.read_csv(args.df_path)
    df = create_class_col(df)
    df["merged_classes"] = LabelEncoder().fit_transform(df["merged_classes"])
    df["PatientID"] = LabelEncoder().fit_transform(df["PatientID"])

    split = list(
        stratified_group_k_fold(
            X=df,
            y=df["merged_classes"],
            groups=df["PatientID"],
            k=args.n_folds,
        )
    )

    np.save(save_path, split)

    print("Done!")
