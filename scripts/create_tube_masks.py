import argparse
import os
import sys

sys.path.append("../")

from PIL import Image
from os.path import join as pjoin
from os.path import splitext
from glob import glob
from tqdm import tqdm

import pandas as pd
import numpy as np

from utils.masking_utils import create_tube_mask


def collect_args():

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument(
        "df_path", type=str, help="Path to anotation dataframes"
    )
    parser.add_argument("root_path", type=str, help="Path to root folder")
    parser.add_argument(
        "mask_folder_path", type=str, help="Path to mask folder"
    )
    parser.add_argument(
        "--save_by_channel",
        type=bool,
        default=False,
        help="Whether to save mask by channel",
    )
    parser.add_argument(
        "--ext", type=str, default=".jpg", help="Image extension"
    )

    args = parser.parse_args()

    return args


if __name__ == "__main__":

    args = collect_args()

    df = pd.read_csv(args.df_path)

    image_names = list(set(df.StudyInstanceUID))

    os.makedirs(args.mask_folder_path)

    for im_name in tqdm(image_names):
        mask = create_tube_mask(
            im_name,
            df,
            args.root_path,
            args.ext,
            save_by_channel=args.save_by_channel,
        )
        if args.save_by_channel:
            np.save(pjoin(args.mask_folder_path, im_name + ".npy"), mask)
        else:
            mask = Image.fromarray(mask)
            mask.save(
                pjoin(args.mask_folder_path, im_name + ".jpeg"),
                "JPEG",
                quality=100,
            )

    print("Ready")
