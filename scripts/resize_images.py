import argparse
import os
import cv2
import numpy as np

from PIL import Image
from os.path import join as pjoin
from os.path import splitext
from glob import glob
from tqdm import tqdm


def collect_args():

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument(
        "image_folder", type=str, help="Path to folder with train/test images"
    )
    parser.add_argument(
        "--target_folder",
        type=str,
        default="",
        help="Path to folder for saving",
    )
    parser.add_argument(
        "--im_height", type=int, default=512, help="Image height"
    )
    parser.add_argument(
        "--im_width", type=int, default=512, help="Image width"
    )
    parser.add_argument(
        "--postfix", type=str, default="jpg", help="jpeg or png or jpg"
    )
    parser.add_argument(
        "--do_best_quality",
        type=bool,
        default=True,
        help="Do high quality resizing",
    )

    args = parser.parse_args()

    return args


if __name__ == "__main__":

    args = collect_args()

    if args.target_folder == "":
        save_folder = args.image_folder + f"_{args.im_height}_{args.im_width}"
    else:
        save_folder = args.target_folder
    os.makedirs(save_folder)

    image_names = glob(pjoin(args.image_folder, "*." + args.postfix))

    for im_name in tqdm(image_names):
        if args.postfix == "npy":
            im = np.load(im_name)
            im = cv2.resize(im, (args.im_height, args.im_width))
            basename = os.path.basename(im_name)
            np.save(pjoin(save_folder, basename), im)
        else:
            im = Image.open(im_name)
            if im.mode != "L":
                im = im.convert("L")
                print("converted!")
            if args.do_best_quality:
                im = im.resize(
                    (args.im_height, args.im_width), Image.ANTIALIAS
                )
                name, _ = splitext(im_name)
                im_name = name + ".jpeg"
                im.save(
                    pjoin(save_folder, os.path.basename(im_name)),
                    "JPEG",
                    quality=100,
                )
            else:
                im = im.resize((args.im_height, args.im_width))
                im.save(pjoin(save_folder, os.path.basename(im_name)))

    print("Ready")
